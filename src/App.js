import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>¡Hola Rubén!</p>
        <a
          className="App-link"
          href="https://nachofmoran.netlify.app/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Portfolio de Nacho
        </a>
      </header>
    </div>
  );
}

export default App;
